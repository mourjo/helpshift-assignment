package helpshift.psil;

/**
 * A wrapper to store the result of an expression and the type of the expression.
 * @author Mourjo Sen
 * @see ExpressionType
 * */

public class ExpressionResult {
	
	private double value;
	private ExpressionType expType;
	
	/**
	 * Create an ExpressionResult object
	 * @param res - Result of the expression
	 * @param e - Type of the expression that produced the result
	 */
	ExpressionResult(double res, ExpressionType e)
	{
		value = res;
		expType = e;
	}
	
	/**
	 * Returns the value of the result. Immutable.
	 * @return value of the result of the expression
	 */
	public double getValue()
	{
		return value;
	}
	
	/**
	 * Returns the type of the expression. Immutable.
	 * @return type of the expression.
	 */
	public ExpressionType getExpressionType()
	{
		return expType;
	}
}

