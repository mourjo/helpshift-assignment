package helpshift.psil;

import java.io.IOException;
/**
 * The main launcher for the word extraction
 * @author Mourjo Sen
 * @see PSilProgram
 */
public class Launcher {

	public static void main(String[] args) throws InvalidPSilProgramException, IOException {
		
		PSilProgram program;
		
		if (args.length > 0)
			program = new PSilProgram(args[0]);
		else
			program = new PSilProgram("input.psil");
		
		System.out.println(program.run());
	}

}
