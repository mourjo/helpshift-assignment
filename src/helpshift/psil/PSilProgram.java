package helpshift.psil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;

/**
* Implementation of an execution engine for running PSIL programs. Call the run method to execute the program.
*
* @author  Mourjo Sen
* @version 1.0
* @since   2015-08-04 
* @see ExpressionType
* @see ExpressionResult
* @see InvalidPSilProgramException
*/
public class PSilProgram {
	private List<String> tokens; // stores the tokens of the program
	private Map<String, Double> symbolTable; // stores the variable names and their current values
	private static final Set<String> operators = new HashSet<String>(); // the allowed operators
	
	static{
		operators.add("+"); // adding the allowed operations
		operators.add("*");
		operators.add("-");
		operators.add("/");
		operators.add("bind");
	}
	
	/**
	 * Takes the path to a PSIL program as input, and does a preliminary parse on the entire program, readies it for execution.
	 * 
	 * @param programPath - Path to the PSIL program
	 * */
	public PSilProgram(String programPath) throws InvalidPSilProgramException, IOException {
		
		BufferedReader input = new BufferedReader(new FileReader(programPath));
		String readyProgram = null;
		
		try
		{
			// parse the file, remove the new line characters
			String line;
			StringBuilder programBuilder = new StringBuilder();
			Pattern spaces = Pattern.compile("[ ]+");
			
			while((line = input.readLine()) != null)
			{
				line = line.replaceAll(String.valueOf((char) 160), " "); // removing non-breaking space
				programBuilder.append(spaces.matcher(line).replaceAll(" "));
				programBuilder.append(" ");
			}
			readyProgram = spaces.matcher(programBuilder).replaceAll(" ");
			
		}
		finally
		{
			input.close();
		}
		
		
		// now parse the program and create the tokens
		tokens = new LinkedList<String>();
		int openBracketsInProgram = 0, closedBracketsInProgram = 0;
		
		for(String t : readyProgram.split(" ")) // space = a delimiter. Therefore space-separated s-expressions are mandatory. See readme.md.
		{
			String res = "";
			int closedBracketsInLine = 0;
			
			for(char c : t.toCharArray())
			{
				if(c != '(' && c != ')') // in between brackets
					res += c; // keep building s-expression
				
				else if (c == '(') // opening bracket found, which is a token
				{
					tokens.add("(");
					openBracketsInProgram++; // count number of open brackets
				}
				
				else if (c == ')') // closing bracket found
				{
					closedBracketsInProgram++; //counting no of closed brackets in program
					closedBracketsInLine++; // counting no of closed brackets in line
				}
			}
			
			tokens.add(res);
			for(int i = 0; i < closedBracketsInLine; i++)
				tokens.add(")");
			
		}
		
		if (openBracketsInProgram != closedBracketsInProgram) // number of opening brackets in a program must equal the number of closing brackets
			throw new InvalidPSilProgramException("Brackets don't match.");
		
		symbolTable = new HashMap<String, Double>(100);
	}
	
	/**
	 * Changes the source of the PSIL program to a new source.
	 * 
	 * @param programPath - Path to the new PSIL program
	 * */
	public void changeSource(String programPath) throws InvalidPSilProgramException, IOException
	{
		// does the same thing as the constructor, without creating a new object
		
		BufferedReader input = new BufferedReader(new FileReader(programPath));
		String readyProgram = null;
		
		try
		{
			// parse the file, remove the new line characters
			String line;
			StringBuilder programBuilder = new StringBuilder();
			Pattern spaces = Pattern.compile("[ ]+");
			
			while((line = input.readLine()) != null)
			{
				line = line.replaceAll(String.valueOf((char) 160), " "); // removing non-breaking space
				programBuilder.append(spaces.matcher(line).replaceAll(" "));
				programBuilder.append(" ");
			}
			readyProgram = spaces.matcher(programBuilder).replaceAll(" ");
			
		}
		finally
		{
			input.close();
		}
		
		// now parse the program and create the tokens
		tokens = new LinkedList<String>();
		int openBracketsInProgram = 0, closedBracketsInProgram = 0;
		
		for(String t : readyProgram.split(" ")) // space = a delimiter. Therefore space-separated s-expressions are mandatory. See readme.md.
		{
			String res = "";
			int closedBracketsInLine = 0;
			
			for(char c : t.toCharArray())
			{
				if(c != '(' && c != ')') // in between brackets
					res += c;
				
				else if (c == '(') // opening bracket found, which is a token
				{
					tokens.add("(");
					openBracketsInProgram++; // count number of open brackets
				}
				
				else if (c == ')') // closing bracket found
				{
					closedBracketsInProgram++; //counting no of closed brackets in program
					closedBracketsInLine++; // counting no of closed brackets in line
				}
			}
			
			tokens.add(res);
			for(int i = 0; i < closedBracketsInLine; i++)
				tokens.add(")");
			
		}
		
		if (openBracketsInProgram != closedBracketsInProgram)
			throw new InvalidPSilProgramException("Brackets don't match.");
		
		symbolTable = new HashMap<String, Double>(100);
	}

	/**
	 * Runs the given program, produces the output on the stdout.
	 * 
	 * @return The value of the last executed s-expression
	 * */
	public double run() throws InvalidPSilProgramException
	{
		// use a stack to process from the most nested s-expression to the outermost s-expression 
		Stack<String> stack = new Stack<String>();
		
		ExpressionResult result = null; // ExpressionResult is used to denote the type of expression it was
		
		for(String token : tokens)
		{
			if (token.equals(")")) // if closing bracket found, an s-expression is complete, so evaluate it
			{
				String exp = "";
				while(stack.peek() != "(") // read the whole expression from the stack
				{
					exp = stack.pop() + " " + exp; // construct an inner s-expression from tokens
					
					if (stack.empty()) // unexpected
						throw new InvalidPSilProgramException("Brackets do not match."); // every opened bracket must match a closing bracket in order as in number
				}
				
				stack.pop(); // pop out the "("
				result = eval(exp); // evaluate the extracted expression
				
				if(result != null && result.getExpressionType() != ExpressionType.bind && result.getExpressionType() != ExpressionType.number)
				{
					// the result of a bind operation does not need to be stored into the stack unless the stack is empty
					if(!stack.empty())
						stack.push(result.getValue() + ""); // store the result back into the stack for outer s-expressions to use it
				}
			}
			else
				stack.push(token); // push token if it is not a bracket, i.e., in between brackets
		}
		
		if (!stack.empty()) // when there are only bind or number s-expressions in the program
		{
			String v = "";
			try
			{
				v = stack.pop();
				if (result == null)
					result = new ExpressionResult(Double.parseDouble(v), ExpressionType.number);
			}
			catch(NumberFormatException nfe)
			{
				if (!symbolTable.containsKey(v))
					throw new InvalidPSilProgramException("Variable " + v + " undefined.");
				throw new InvalidPSilProgramException("There is an error in the program.");
			}
		}
		
		if (result == null)
			throw new InvalidPSilProgramException();
		
		return result.getValue();
	}
	
	/**
	 * Evaluates a given s-expression.
	 * 
	 * @param expression - The expression to be evaluated
	 * @return The result of the expression wrapped in an ExpressionResult object
	 * 
	 * @see ExpressionResult
	 * */
	private ExpressionResult eval(String expression) throws InvalidPSilProgramException
	{
		expression = expression.trim();
		
		if(expression.length() < 1) //empty
			throw new InvalidPSilProgramException("Empty s-expression not allowed.");
		
		String elements[] = expression.split(" ");

		if(elements.length < 1) //another level of checking emptiness
			throw new InvalidPSilProgramException("Empty s-expression not allowed.");
		
		if(elements[0].charAt(0) == '*') // check type of expression
		{
			if(elements.length < 3) // less than 2 operands given
				throw new InvalidPSilProgramException("Multiplication operator needs at least 2 operands.");
			
			double res = 1d; // accumulator
			for(int i = 1; i < elements.length; i++)
			{
				try
				{
					res *= Double.parseDouble(elements[i]); // (* a b c d) means a * b * c * d
				}
				catch(NumberFormatException nfe) // maybe a previously initialized variable is being accessed
				{
					if (operators.contains(elements[i]))// variable names cannot be operators
						throw new InvalidPSilProgramException("Operator " + elements[i] + " cannot be used inside an s-expression.");
					
					if(symbolTable.get(elements[i]) == null) // variable undefined
						throw new InvalidPSilProgramException("Variable " + elements[i] + " not defined.");
					
					res *= symbolTable.get(elements[i]); // (* a b c d) means a * b * c * d
				}
			}
			return new ExpressionResult(res, ExpressionType.multiply);
		}
		else if(elements[0].charAt(0) == '+') // check type of expression
		{
			if(elements.length < 3)
				throw new InvalidPSilProgramException("Addition operator needs at least 2 operands.");
			
			double res = 0d; // accumulator
			for(int i = 1; i < elements.length; i++)
			{
				try
				{
					res += Double.parseDouble(elements[i]); // (+ a b c d) means a + b + c + d
				}
				catch(NumberFormatException nfe) // maybe a previously initialized variable is being accessed
				{
					if (operators.contains(elements[i]))// variable names cannot be operators
						throw new InvalidPSilProgramException("Operator " + elements[i] + " cannot be used inside an s-expression.");
					
					if(symbolTable.get(elements[i]) == null) // variable undefined
						throw new InvalidPSilProgramException("Variable " + elements[i] + " not defined.");
					
					res += symbolTable.get(elements[i]); // (+ a b c d) means a + b + c + d
				}
			}
			return new ExpressionResult(res, ExpressionType.add);
			
		}
		else if(elements[0].charAt(0) == '-') // check type of expression
		{
			if(elements.length < 3)
				throw new InvalidPSilProgramException("Subtraction operator needs at least 2 operands.");
			
			double res = 0d; // accumulator
			for(int i = 1; i < elements.length; i++)
			{
				try
				{
					 // (- a b c d) means a - b - c - d
					if (i == 1)
						res += Double.parseDouble(elements[i]);
					else
						res -= Double.parseDouble(elements[i]);
				}
				catch(NumberFormatException nfe) // maybe a previously initialized variable is being accessed
				{
					if (operators.contains(elements[i])) // variable names cannot be operators
						throw new InvalidPSilProgramException("Operator " + elements[i] + " cannot be used inside an s-expression.");
					
					if(symbolTable.get(elements[i]) == null) // variable undefined
						throw new InvalidPSilProgramException("Variable " + elements[i] + " not defined.");
					
					// (- a b c d) means a - b - c - d
					if (i == 1)
						res += symbolTable.get(elements[i]);
					else
						res -= symbolTable.get(elements[i]);
				}
			}
			return new ExpressionResult(res, ExpressionType.subtract);
			
		}
		else if(elements[0].charAt(0) == '/') // check type of expression
		{
			if(elements.length < 3)
				throw new InvalidPSilProgramException("Division operator needs at least 2 operands.");
			
			double res = 0d; // accumulator
			for(int i = 1; i < elements.length; i++)
			{
				try
				{
					// (/ a b c d) means (((a / b) / c) / d)
					if (i == 1)
						res += Double.parseDouble(elements[i]);
					else
						res /= Double.parseDouble(elements[i]);
				}
				catch(NumberFormatException nfe) // maybe a previously initialized variable is being accessed
				{
					if (operators.contains(elements[i])) // variable names cannot be operators
						throw new InvalidPSilProgramException("Operator " + elements[i] + " cannot be used inside an s-expression.");
					
					if(symbolTable.get(elements[i]) == null) // variable undefined
						throw new InvalidPSilProgramException("Variable " + elements[i] + " not defined.");
					
					// (/ a b c d) means (((a / b) / c) / d)
					if (i == 1)
						res += symbolTable.get(elements[i]);
					else
						res /= symbolTable.get(elements[i]);
				}
			}
			return new ExpressionResult(res, ExpressionType.divide);
			
		}
		else if(elements[0].equals("bind")) // check type of expression
		{
			if(elements.length != 3)
				throw new InvalidPSilProgramException("Bind expects exactly 2 arguments.");
			
			Double value = -1d;
			try
			{
				if (operators.contains(elements[1]))
					throw new InvalidPSilProgramException("Operator " + elements[1] + " cannot be used inside an s-expression.");
				
				if(!elements[1].matches("[a-zA-Z]+")) // disallowed characters for variable names
					throw new InvalidPSilProgramException("Variable names can only contain A-Z and a-z.");
				
				value = Double.parseDouble(elements[2]);
				symbolTable.put(elements[1], value);
			}
			catch(NumberFormatException nfe) // maybe a previously initialized variable is being accessed
			{
				if (operators.contains(elements[2]))// variable names cannot be operators
					throw new InvalidPSilProgramException("Operator " + elements[2] + " cannot be used inside an s-expression.");
				
				if((value = symbolTable.get(elements[2])) == null) // variable undefined
					throw new InvalidPSilProgramException("Variable " + elements[2] + " not defined.");
				
				symbolTable.put(elements[1], value); // add variable, value to symbol table
			}
			return new ExpressionResult(value, ExpressionType.bind);
		}
		else // no operator
		{
			if(elements.length > 1)
				throw new InvalidPSilProgramException("No operator found for operands in s-expression.");
			
			try
			{
				return new ExpressionResult(Double.parseDouble(elements[0]), ExpressionType.number);
			}
			catch(NumberFormatException nfe) // maybe a previously initialized variable is being accessed
			{
				if(symbolTable.get(elements[0]) == null) // variable undefined
					throw new InvalidPSilProgramException("Variable " + elements[0] + " not defined.");
				
				return new ExpressionResult(symbolTable.get(elements[0]), ExpressionType.number);
			}
		}
	}
}
