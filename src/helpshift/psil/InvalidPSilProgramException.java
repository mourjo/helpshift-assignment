package helpshift.psil;

/**
 * An exception class that is used to throw exceptions when the
 * PSIL program is invalid.
 * @author Mourjo Sen
 * */
public class InvalidPSilProgramException extends Exception {

	private static final long serialVersionUID = 6997986485211289986L;

	public InvalidPSilProgramException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidPSilProgramException(String arg0) {
		super(arg0);
	}

	public InvalidPSilProgramException(Throwable arg0) {
		super(arg0);
	}

	public InvalidPSilProgramException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public InvalidPSilProgramException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
