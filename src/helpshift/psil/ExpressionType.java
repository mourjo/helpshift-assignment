package helpshift.psil;
/**
 * Used to determine the type of an expression that was just evaluated
 * @author Mourjo Sen
 *
 */
public enum ExpressionType {
	add, multiply, subtract, divide, bind, number
}
