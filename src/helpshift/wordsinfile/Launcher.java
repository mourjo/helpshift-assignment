package helpshift.wordsinfile;

import java.io.IOException;


/**
 * The main launcher for the word extraction
 * @author Mourjo Sen
 * @see WordStore
 * @see WordFrequencyPair
 * @see UniquePriorityQueue
 */
public class Launcher {

	public static void main(String args[]) throws IOException, MemoryInsufficientException
	{
		WordStore ws;
		if (args.length > 0)
			ws = new WordStore(args[0]); // name of the file to be parsed taken from command line
		else
			ws = new WordStore("all_shakespeare.txt"); // default name of the file to be parsed
		ws.findFrequency();
	}

}
