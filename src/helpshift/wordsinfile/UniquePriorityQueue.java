package helpshift.wordsinfile;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * An extension to the PriorityQueue of Java 1.8
 * Used to store only unique WordFrequencyPair objects
 * @author Mourjo Sen
 * @see WordFrequencyPair
 */

public class UniquePriorityQueue extends PriorityQueue<WordFrequencyPair>{

	private static final long serialVersionUID = 1L;
	private HashSet<String> wordSet; // keeps track of the uniqueness of the elements of the heap. String to minimize memory footprint.
	
	/**
	 * Creates a PriorityQueue
	 */
	public UniquePriorityQueue()
	{
		super();
		wordSet = new HashSet<String>();
	}
	
	/**
	 * Create the PriorityQueue with an initial capacity and a comparator (only supported in Java 1.8)
	 * @param initialCapacity - The initial capacity of the heap
	 * @param comparator - The Comparator used to enter elements into the heap
	 */
	UniquePriorityQueue(int initialCapacity, Comparator<WordFrequencyPair> comparator)
	{
		// only in Java 8
		super(initialCapacity, comparator);
		wordSet = new HashSet<String>(initialCapacity);
	}
	
	UniquePriorityQueue(int initialCapacity)
	{
		super(initialCapacity);
		wordSet = new HashSet<String>(initialCapacity);
	}
	
	@Override
	public boolean add(WordFrequencyPair element)
	{
		String word = element.getWord();
		if(wordSet.contains(word))
			return false; // cannot add, uniqueness property violated
		wordSet.add(word); // add to the set of words seen
		super.add(element); // add to the heap
		return true;
	}
	
	
	@Override
	public WordFrequencyPair remove()
	{
		WordFrequencyPair top = super.remove(); // remove top element from heap
		wordSet.remove(top.getWord()); // remove from set
		return top; // return removed object
	}
	
	
	@Override
	public boolean remove(Object wordFrequencyObject)
	{
		if(!(wordFrequencyObject instanceof WordFrequencyPair))
			return false; // bad request
		
		if(wordSet.remove(((WordFrequencyPair)wordFrequencyObject).getWord())) // if word found in set (ie was inserted before)
			return super.remove(wordFrequencyObject); // remove from heap
		
		return false;
	}
	
	@Override
	public boolean contains(Object wordFrequencyObject)
	{
		if(!(wordFrequencyObject instanceof WordFrequencyPair))
			return false; // bad request
		return wordSet.contains(((WordFrequencyPair)wordFrequencyObject).getWord()); // almost O(1) complexity
	}
	
	/**
	 * Adds the passed amount to the frequency of an element already present in the heap
	 * @param word  - The item whose frequency is to be increased
	 * @param frequency - The value by which it is to be increased
	 * @return true if the value was found, false otherwise
	 */
	public boolean addToFrequency(String word, int frequency)
	{
		if (wordSet.contains(word))
		{
			Iterator<WordFrequencyPair> it = super.iterator();
			while(it.hasNext()) // cannot be better than O(n), somewhere something had to be O(n)
			{
				WordFrequencyPair wfp = it.next();
				if(wfp.getWord().equals(word))
				{
					wfp.setFrequency(frequency + wfp.getFrequency());
					return true; // due to guaranteed uniqueness, no need to iterate further
				}
			}
		}
		
		return false; // not found
	}
	
}
