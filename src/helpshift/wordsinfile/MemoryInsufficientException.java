package helpshift.wordsinfile;

/**
 * 
 * Exception when memory is insufficient. OutOfMemoryError should not be thrown by user code.
 * @author Mourjo Sen
 *
 */
public class MemoryInsufficientException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public MemoryInsufficientException() {
		super();
	}

	public MemoryInsufficientException(String arg0) {
		super(arg0);
	}

	public MemoryInsufficientException(Throwable arg0) {
		super(arg0);
	}

	public MemoryInsufficientException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public MemoryInsufficientException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}
}
