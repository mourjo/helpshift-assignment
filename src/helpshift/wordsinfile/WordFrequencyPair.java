package helpshift.wordsinfile;

import java.util.regex.Pattern;


/**
 * Stores a word and its frequency in the same object
 * @author Mourjo Sen
 *
 */
public class WordFrequencyPair implements Comparable<WordFrequencyPair>{
	private String word; // immutable
	private int frequency; // can be altered
	static final Pattern commaPattern = Pattern.compile(",");
	
	/**
	 * Create a WordFrequencyPair object
	 * @param word - The word
	 * @param frequency - The frequency of the word
	 */
	public WordFrequencyPair(String word, int frequency)
	{
		this.word = word;
		this.frequency = frequency;
	}
	
	/**
	 * Create a WordFrequencyPair object from a line of text by exploding the line
	 * @param lineFromFile - The line from which to extract the words
	 */
	public WordFrequencyPair(String lineFromFile)
	{
		String w[] = commaPattern.split(lineFromFile); 
		int freq = Integer.parseInt(w[1]);
		this.word = w[0];
		this.frequency = freq;
	}
	
	/**
	 * Get the frequency of the word
	 * @return The frequency of the word
	 */
	public int getFrequency()
	{
		return frequency;
	}
	
	/**
	 * Change the frequency of the current word
	 * @param frequency - The new value for the frequency
	 */
	public void setFrequency(int frequency)
	{
		this.frequency = frequency;
	}
	
	/**
	 * Due to immutability, accessor method is required to access the word
	 * @return The word
	 */
	public String getWord()
	{
		return word;
	}
	
	@Override
	public String toString()
	{
		return word + "," + frequency;
	}

	@Override
	public int compareTo(WordFrequencyPair other) {
		// needed for treeset and heap
		return word.compareTo(other.word);
	}
	
	@Override
	public int hashCode()
	{
		return word.hashCode(); // needed for hashset and hashmap
	}
	
	@Override
	public boolean equals(Object other)
	{
		// needed for hashset and hash map
		if(!(other instanceof WordFrequencyPair))
			return false;
		
		return word.equals(((WordFrequencyPair) other).word);
		
	}
}
