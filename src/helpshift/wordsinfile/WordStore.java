package helpshift.wordsinfile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.TreeMap;
import java.util.regex.Pattern;

/**
 * 
 * Reads a file and finds the most and least frequently occurring words in it
 * @author Mourjo Sen
 * @see WordFrequencyPair
 * @see UniquePriorityQueue
 */

public class WordStore {
	
	private String inputFilePath;
	private int temporaryFileCount; 
	private static final Runtime runtime = Runtime.getRuntime();
	private static final String consolidatedFilePath = "tmp/full.txt";
	private static final String tempFilesPath = "tmp/";
	
	
	/**
	 * Constructs the WordStore object.
	 * 
	 * @param Path to the input file to be analyzed.
	 * @throws MemoryInsufficientException 
	 * */
	public WordStore(String inputFilePath) throws MemoryInsufficientException
	{
		this.inputFilePath = inputFilePath;
		temporaryFileCount = 0;
	}
	
	
	/**
	 * Changes the input file from the existing one to a new file.
	 * 
	 * @param newInputFilePath - Path to the new input file.
	 * */
	public void changeInputFile(String newInputFilePath)
	{
		this.inputFilePath = newInputFilePath;
		temporaryFileCount = 0;
	}
	
	
	/**
	 * Determines if mapping to the disk is required at the current time.
	 * 
	 * @return A boolean value which is true if mapping to disk is required.
	 * */
	private boolean diskMappingRequired()
	{
		// return true if the available memory is less than 3 MB
		return (double)runtime.freeMemory()/(1024.0*1024.0) < 3D;
	}
	
	
	/**
	 * Loads the stop words from stopwords.txt, which has to be present in the classpath.
	 * 
	 * @return A hash set containing the stop words.
	 * */
	private HashSet<String> loadStopWords(String path) throws IOException
	{
		HashSet<String> stopWords = new HashSet<String>();
		BufferedReader file = new BufferedReader(new FileReader("stopwords.txt"));
		try
		{
			String line;
			while((line = file.readLine()) != null)
				stopWords.add(line.trim().toLowerCase());
			
		}
		finally
		{
			file.close();
		}
		return stopWords;
	}
	
	/**
	 * A public method to find the top 5 and bottom 5 frequencies in the current file.
	 * Prints the most and least frequent words in the file to the stdout.
	 * @throws MemoryInsufficientException 
	 * */
	public void findFrequency() throws IOException, MemoryInsufficientException
	{
		findFrequency(5); // overloaded call
	}
	
	/**
	 * A public method to find the top num and bottom num frequencies in the current file.
	 * Prints the most and least frequent words in the file to the stdout.
	 * 
	 * @param num - Number of top/bottom words by frquency to be printed.
	 * @throws MemoryInsufficientException 
	 * */
	public void findFrequency(int num) throws IOException, MemoryInsufficientException
	{
		TreeMap<String, Integer> dict = new TreeMap<String, Integer>();
		BufferedReader inputFile = new BufferedReader(new FileReader(inputFilePath));
		
		Pattern wordSplitter = Pattern.compile("[^A-Za-z0-9']"); // pattern to split words
		HashSet<String> stopWords = loadStopWords("stopwords.txt");
		
		boolean diskMapping = false; // keeps track if disk mapping was required
		
		try
		{
			String line;
			while ((line = inputFile.readLine()) != null)
			{
				line = line.toLowerCase();
				for(String word : wordSplitter.split(line))
				{
					word = word.trim(); //remove leading and trailing white spaces
					
					if(word.length() > 1 && word.startsWith("\'"))
						word = word.substring(1);
					
					if(word.endsWith("\'"))
						word = word.substring(0,word.length()-1);
					
					//if it is not a stopword or just a '
					if(!stopWords.contains(word) && (word.length() > 1 || (word.length() == 1 && !word.contains("\'"))))
					{
						if(dict.containsKey(word))
							dict.put(word, dict.get(word) + 1);
						else
							dict.put(word, 1);
					}
				}

				if (diskMappingRequired())
				{
					diskMapping = true;
					mapToDisk(dict);
				}
			}
			
			if(diskMapping)
			{
				mapToDisk(dict);
				
				consolidateMappedFiles();
				
				findMostAndLeastFrequentWords(num);
			}
			else
			{
				findMostAndLeastFrequentWords(dict, num);
			}
		}
		finally
		{
			inputFile.close();
		}
	}
	
	
	/**
	 * Given a dictionary of sorted words, finds and prints the most and least frequently occurring words.
	 * 
	 * @param dict - Sorted dictionary containing the number of occurrences of each word.
	 * @param quantity - Number of most/least frequent words to be found.
	 * */
	private void findMostAndLeastFrequentWords(TreeMap<String, Integer> dict, int quantity)
	{
		if (dict.size() <= 0)
			return;
		
		PriorityQueue<WordFrequencyPair> maxHeap = new PriorityQueue<WordFrequencyPair>(quantity, new Comparator<WordFrequencyPair>(){

			@Override
			public int compare(WordFrequencyPair pair1, WordFrequencyPair pair2) {
				return ((Integer)pair2.getFrequency()).compareTo(pair1.getFrequency());
			}});
			
			
		PriorityQueue<WordFrequencyPair> minHeap = new PriorityQueue<WordFrequencyPair>(quantity, new Comparator<WordFrequencyPair>(){

			@Override
			public int compare(WordFrequencyPair pair1, WordFrequencyPair pair2) {
				return ((Integer)pair1.getFrequency()).compareTo(pair2.getFrequency());
			}});
		
		while(dict.size() > 0)
		{
			Entry<String, Integer> curr = dict.pollFirstEntry();
			
			WordFrequencyPair current = new WordFrequencyPair(curr.getKey(), curr.getValue());
			if (minHeap.size() < quantity)
				minHeap.add(current);
			else
			{
				WordFrequencyPair minWord = minHeap.element();
				
				if(minWord.getFrequency() < current.getFrequency())
				{
					minHeap.remove();
					minHeap.add(current);
				}
			}
			
			
			if (maxHeap.size() < quantity)
				maxHeap.add(current);
			else
			{
				WordFrequencyPair maxWord = maxHeap.element();
				
				if(maxWord.getFrequency() > current.getFrequency())
				{
					maxHeap.remove();
					maxHeap.add(current);
				}
			}
		}
		
		System.out.println("Most frequent words:");
		for(WordFrequencyPair wfp : minHeap)
			System.out.println(wfp.getWord());
		
		
		System.out.println("\n\nLeast frequent words:");
		for(WordFrequencyPair wfp : maxHeap)
			System.out.println(wfp.getWord());
		
		new File(tempFilesPath + "full.txt").delete();
	}
	
	
	/**
	 * Finds the most and least frequently occurring words when the whole file cannot be loaded into memory.
	 * Reads the temporary files in the tmp directory to reconstruct the dictionary one entry at a time
	 * by parsing the temporary files.
	 * 
	 * @param quantity - Number of most/least frequent words to be found.
	 * */
	private void findMostAndLeastFrequentWords(int quantity)throws IOException
	{
		
		/* use maxheap for storing least 5
		 * use minheap for storing max 5*/
		
		PriorityQueue<WordFrequencyPair> maxHeap = new PriorityQueue<WordFrequencyPair>(quantity, new Comparator<WordFrequencyPair>(){

			@Override
			public int compare(WordFrequencyPair pair1, WordFrequencyPair pair2) {
				return ((Integer)pair2.getFrequency()).compareTo(pair1.getFrequency());
			}});
			
			
		PriorityQueue<WordFrequencyPair> minHeap = new PriorityQueue<WordFrequencyPair>(quantity, new Comparator<WordFrequencyPair>(){

			@Override
			public int compare(WordFrequencyPair pair1, WordFrequencyPair pair2) {
				return ((Integer)pair1.getFrequency()).compareTo(pair2.getFrequency());
			}});
		
		
		BufferedReader consolidatedFile = new BufferedReader(new FileReader (consolidatedFilePath));
		
		
		try
		{
			String line;
			while((line = consolidatedFile.readLine()) != null)
			{
				if (minHeap.size() < quantity)
					minHeap.add(new WordFrequencyPair(line));
				else
				{
					WordFrequencyPair minWord = minHeap.element();
					WordFrequencyPair wfq = new WordFrequencyPair(line);
					
					if(minWord.getFrequency() < wfq.getFrequency())
					{
						minHeap.remove();
						minHeap.add(wfq);
					}
				}
				
				
				if (maxHeap.size() < quantity)
					maxHeap.add(new WordFrequencyPair(line));
				else
				{
					WordFrequencyPair maxWord = maxHeap.element();
					WordFrequencyPair wfq = new WordFrequencyPair(line);
					
					if(maxWord.getFrequency() > wfq.getFrequency())
					{
						maxHeap.remove();
						maxHeap.add(wfq);
					}
				}
			}
			
			System.out.println("Most frequent words:");
			for(WordFrequencyPair wfp : minHeap)
				System.out.println(wfp.getWord());
			
			
			System.out.println("\n\nLeast frequent words:");
			for(WordFrequencyPair wfp : maxHeap)
				System.out.println(wfp.getWord());
			
		}
		finally
		{
			consolidatedFile.close();
		}
	}
	
	
	/**
	 * Merges the disk-mapped files to construct a file containing one entry per word in the input file.
	 * @throws MemoryInsufficientException 
	 * */
	private void consolidateMappedFiles() throws IOException, MemoryInsufficientException
	{
		PrintWriter fullFile = new PrintWriter(new BufferedWriter(new FileWriter(consolidatedFilePath)));
		List<BufferedReader> infileList = new ArrayList<BufferedReader>();
		try
		{
			int finishedFiles = 0;
			HashMap<String, List<Integer>> filemap = new HashMap<String, List<Integer>>();
			
			UniquePriorityQueue heap = new UniquePriorityQueue();
			for(int i = 0; i < temporaryFileCount; i++)
			{
				infileList.add(new BufferedReader(new FileReader(tempFilesPath + i + ".txt")));
				String line = infileList.get(i).readLine();
				
				String w[] = line.split(","); 
				int freq = Integer.parseInt(w[1]);

				if(!heap.add(new WordFrequencyPair(line)))
				{
					heap.addToFrequency(w[0], freq);
					filemap.get(w[0]).add(i);
				}
				else
				{
					List<Integer> llist = new ArrayList<Integer>();
					llist.add(i);
					filemap.put(w[0], llist);
				}
			}
			
			while(finishedFiles < temporaryFileCount)
			{
				WordFrequencyPair wfp = heap.remove();
				
				fullFile.println(wfp);
				
				String word = wfp.getWord();
				
				
				for(int i : filemap.get(word))
				{
					String line;
					if((line = infileList.get(i).readLine()) != null)
					{
						String w[] = line.split(","); 
						int freq = Integer.parseInt(w[1]);

						if(!heap.add(new WordFrequencyPair(line)))
						{
							heap.addToFrequency(w[0], freq);
							filemap.get(w[0]).add(i);
						}
						else
						{
							List<Integer> llist = new ArrayList<Integer>();
							llist.add(i);
							filemap.put(w[0], llist);
						}
					}
					else finishedFiles++;
				}
				
				filemap.remove(word);
			}
		}
		catch(Exception e)
		{
			System.err.println(e);
		}
		finally
		{
			try
			{
				for(int i = 0; i < temporaryFileCount; i++)
					infileList.get(i).close();
			}
			catch (IndexOutOfBoundsException e)
			{
				fullFile.close();
				cleanup();
				
				throw new MemoryInsufficientException("Memory too less to compute frequencies");
			}
			fullFile.close();
		}
		cleanup();
	}
	
	
	/**
	 * Given a dictionary of sorted words, writes it to the disk, freeing memory to read the rest of the input file.
	 * 
	 * @param dict - Sorted dictionary containing the number of occurrences of words
	 * */
	private void mapToDisk(TreeMap<String, Integer> dict) throws IOException
	{
		if(dict.size() > 0)
		{
			PrintWriter diskMappedDictFile = new PrintWriter(new BufferedWriter(new FileWriter(tempFilesPath + temporaryFileCount+".txt")));
			try
			{
				temporaryFileCount++;
				
				for(String key : dict.keySet())
					diskMappedDictFile.println(key+","+dict.get(key));
				dict.clear();
			}
			finally
			{
				diskMappedDictFile.close();
			}
		}
	}
	
	/**
	 * Cleans up the temporary files left behind after mapping to disk.
	 * */
	private void cleanup()
	{
		for(int i = 0; i < temporaryFileCount; i++)
			new File(tempFilesPath + i + ".txt").delete();
	}
}
