# Solution to the **Helpshift Assignment**
**Important:** *Requires Java 1.8 or higher*

For the two questions, there are two packages in the project: `helpshift.psil` and `helpshift.wordsinfile`. 
Both these packages have a Launcher class which should be used to run the program. 

## Setting up the project

**Requires *Java 1.8+*.**

First, clone this repository. The project directory is organized as follows:
```
.
├── all_shakespeare.txt
├── Helpshift-Take-Home-Task-v1.0.1.pdf
├── psil_input.txt
├── readme.md
├── bin
├── tmp
├── src
│   └── helpshift
│       ├── psil
│       │   ├── ExpressionResult.java
│       │   ├── ExpressionType.java
│       │   ├── InvalidPSilProgramException.java
│       │   ├── Launcher.java
│       │   └── PSilProgram.java
│       └── wordsinfile
│           ├── Launcher.java
│           ├── UniquePriorityQueue.java
│           ├── WordFrequencyPair.java
│           └── WordStore.java
└── stopwords.txt

```
Second, navigate to the root of the project.
Third, import it in Eclipse to run it within the IDE.

Optionally, you can run the word extraction problem (first question) from the command line (at the root of the project tree) as follows:
```
javac -d bin src/helpshift/wordsinfile/*.java
java -cp bin helpshift.wordsinfile.Launcher
```
To run the PSil execution problem (second question) from the command line do the following from root of the project tree:
```
javac -d bin src/helpshift/psil/*.java
java -cp bin helpshift.psil.Launcher
```
For both cases above, you can pass the filename of the input file via a command line argument.


## Notes
The assumptions I made related to the problems and some clarifications are stated in the following subsections.
#### Word Frequency Problem
* The question said that it may not be possible to load the input file into memory as a whole. But it may be possible to load the _unique_ words into memory to count their
frequencies. This is what the program tries to do. If while doing this it finds that it is running out of memory, then it divides the input file into chunks and for each chunk, 
it finds the frequency of each word. Then these files are merged. This will work in most cases, but fail in a case when the memory available is so less that the merging operation
is not possible, that is, the available memory is so low that n files cannot be opened at the same time. A `MemoryInsufficientException` is thrown in this case.
* The word extraction process is done using non alphanumeric characters. That is, all special characters except `'` are treated as delimiters (like `space`, `,` etc.).
* It is assumed that the `stopwords.txt` file and the `tmp` directory will be present in the root of the project directory.
* It is advised to use Oracle JDK (1.8) instead of OpenJDK (1.8) because the Oracle JDK is manages memory much better than OpenJDK.
* The default filename is `all_shakespeare.txt`. The input filename can be passed as a command line argument as:
```
javac -d bin src/helpshift/wordsinfile/*.java
java -cp bin helpshift.wordsinfile.Launcher /path/to/new/input.txt
```

#### PSil Execution Problem
* The program syntax was assumed to enforce space separated atoms. That is, `(+1 2 3)` is not allowed. Operators and numbers **must** be separated by a space. 
That is, the correct syntax for the above would be `(+ 1 2 3)`. Note the space after the `+`. If this space is absent, the program will ignore the first operand. 
This is the format used in the problem description.
* I have provided support for `-` and `/` operations as well as for negative numbers and fractions. The assumptions in this regard are as follows:
	* `(- 1 2 3.55 4.66)` means `1 - 2 - 3.55 - 4.66` which evaluates to `-9.21`
	* `(/ 1 2 -3 -4)` means `((1 / 2) / -3) / -4` which evaluates to `0.0416666`. Precedence is left-to-right.
	* And because division has a special case, `(/ 1 8 0 9 7 6 5 4)` evaluates to `Infinity`.
* When there is a syntax error in the program (other than the first point), the program throws an `InvalidPSilProgramException`. It also mentions why the input is invalid.
	* For example, `(/ 1 )` gets a `InvalidPSilProgramException` exception saying `Division operator needs at least 2 operands`.
* The default input file is `input.psil` and it is expected to be present in the root of the project directory.
* The input filename can be passed as a command line argument as:
```
javac -d bin src/helpshift/psil/*.java
java -cp bin helpshift.psil.Launcher /path/to/new/input.psil
```
#### Mistake in the problem statement
In the problem statement pdf, the following was given as a valid program, but it missed a closing `)` and is thus not a valid program.
```
(bind length 10) 
(bind breadth (+ length 1) 
(bind length 11) 
(* length breadth)
``` 
But after adding the `)`, the program evaluates to `121` as expected. The corrected version:
```
(bind length 10) 
(bind breadth (+ length 1) )
(bind length 11) 
(* length breadth)
``` 
#### Javadoc
The project is documented with inline comments and Javadoc comments explaining the operations from time to time.